Title: Swallow the Sea
Date: 2023-02-20 04:00
Category: review/pc
Tags: review, pc, 6/10
Authors: zarlo
Summary: Swallow the Sea review
Score: 6
developer: Talia bob Mair, Nicolás Delgado
publisher: ItsTheTalia
game_link: https://store.steampowered.com/app/1511860/Swallow_the_Sea/
---

Its a fun short game about 15 minutes you play as a cannibalistic fetus in a surreal world moving from room to room trying to not get eaten by larger creatures while eating smalller ones
the art style sets the unsettling atmosphere very well and its free
