Title: Team Fortress 2
Date: 2023-02-19 00:00
Category: review/pc
Tags: review, pc, fps, 8/10, multiplayer
Authors: zarlo
Summary: Team Fortress 2 review
Score: 8
developer: Valve
game_link: https://store.steampowered.com/app/440/Team_Fortress_2/
---

Team Fortress 2 is a first person shooter developed by valve was released on 10th of October 2007 its build on the source engine there are 9 class'es all with different health, speed and hitboxes.

The scout is the fastest class is able to double jump and has double capture speed and weapons include a shotgun, pistol and a bat.

The soldier he uses a rocket launcher, shotgun and a Shovel the soldier is able to use his rocket launcher to improve his movement but fireing a rocket at the floor while jumping.

The pyro has a flame thrower, shotgun and a fire axe the pyro can use its flame thrower secondary fire to blast compressed air which knocks back enemies, redirects enemy projectiles, and extinguishes flames on teammates but at the cost of ammo this class it aslo the most common class used for spy checking.

The demoman has a grenade launcher, stickybomb launcher and a bottle he is able to use his stickybomb launcher to set traps that he can trigger latter.

The heavy is the slowest and largest on the team and uses a minigun, shotgun and uses his fists the heavy works as a good meat shield for his team mates.

The engineer has a shotgun, pistol and a wrench along with 2 PDA's a construction one and a destruction one he uses the construction PDA to build his sentry, dispenser, and teleporter.

The medic has a syringe gun, medi gun and a bonesaw but when playing you will mostly just use the medi gun his primary job is to heal his team mates and when healing his team mate if the player you are healing is fastre then you you will match their speed while healing them.

There are many games modes like Control Point, CTF, King of the Hill, some are seasonal like Player Destruction. most games are 12v12.


[Set up a tf2 Server]({filename}/guide/tf2_server.md)
