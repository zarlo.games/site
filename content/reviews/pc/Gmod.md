Title: Garry's Mod
Date: 2023-02-19 00:00
Category: review/pc
Tags: review, pc, fps, 8/10, multiplayer
Authors: zarlo
Summary: Garry's Mod review
Score: 8
developer: Facepunch Studios
publisher: Valve
game_link: https://store.steampowered.com/app/4000/Garrys_Mod/
---

Garry's Mod is a sandbox game made in the source engine most of the time you will not be playing the vanilla game but on one of the many servers with a large range of custom made game mods the work shop has a large range of addons all made in lua if you joina server that uses a mod you client will download then needed addons for you either from the workshop or from the server you are connecting to
