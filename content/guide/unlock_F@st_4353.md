Title: unlock your F@st 4353 
Date: 2023-08-04 00:00
Category: guide/modem
Tags: guide, modem, hack, freedom
Authors: zarlo
Summary: unlock SSH on your F@st 4353 modem
---

Unlock your F@st 4353 modem

alot of modems given to customers from ISP are locked down i don't like this this is a guide you fix that on a Sagemcom Broadband F@st 4353 this my work on others too

login to you modem then open the javascript console

Enable the remote access

```js
$.xmo.setValuesTree(true, "Device/UserAccounts/Users/User[@uid='6']/RemoteAccesses/RemoteAccess[@uid='2']/Enabled");
```

Give it a port

```js
$.xmo.setValuesTree(22, "Device/UserAccounts/Users/User[@uid='6']/RemoteAccesses/RemoteAccess[@uid='2']/Port");
```

Now let lowlife router owners log in via Lan

```js
$.xmo.setValuesTree("ACCESS_ENABLE_ALL", "Device/UserAccounts/Users/User[@uid='6']/RemoteAccesses/RemoteAccess[@uid='2']/LANRestriction");
```

Set the SSH password

```js
$.xmo.setValuesTree("assist", "Device/UserAccounts/Users/User[@uid='6']/ClearTextPassword");
```

And reboot, when it comes back up you'll have a SSH server waiting on port 22. Login with assist:assist. Once in 'su' and the password is root. And you have control of your router again.

```bash
ssh -oKexAlgorithms=+diffie-hellman-group1-sha1 assist@[gate way IP]
```

Once logged in you can do things like change you dns servers

```bash
xmo-client -s "1.1.1.1" -p 'Device/DHCPv4/Server/Pools/Pool[@uid="1"]/DNSServers'
```
