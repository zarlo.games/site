AUTHOR = 'zarlo'
SITENAME = 'Zarlos Games'
SITEURL = 'http://127.0.0.1:8000/'

THEME = './theme'


PLUGINS = [
    'more_categories', 
    'search', 
    'sitemap', 
    'seo',
    'pelican.plugins.webassets'
]

PATH = 'content'

TIMEZONE = 'Australia/Sydney'

DEFAULT_DATE_FORMAT = '%Y/%m/%d'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
MENUITEMS = (
    ('Steam Account', 'https://steamcommunity.com/id/zarlo5899/'),
    ('Reviews', '/tag/review'),
    ('Guides', '/tag/guide'),
)

DEFAULT_PAGINATION = 10

MAIN_MENU = True
DISABLE_URL_HASH = True

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'use_pygments': True},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'pymdownx.blocks.tab': {},
        'pymdownx.blocks.html': {},
        'pymdownx.magiclink': {},
        
    },
    'output_format': 'html5',
}

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

SITEMAP = {
    "format": "xml",
    "priorities": {
        "articles": 0.5,
        "indexes": 0.5,
        "pages": 0.5
    },
    "changefreqs": {
        "articles": "monthly",
        "indexes": "daily",
        "pages": "monthly"
    }
}

ARTICLE_URL = 'posts/{date:%Y}/{slug}-{date:%m}{date:%d}'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{slug}-{date:%m}{date:%d}.html'
PAGE_URL = 'pages/{slug}'
PAGE_SAVE_AS = 'pages/{slug}.html'

DRAFT_URL = 'drafts/{date:%Y}/{slug}-{date:%m}{date:%d}'
DRAFT_SAVE_AS = 'drafts/{date:%Y}/{slug}-{date:%m}{date:%d}.html'

AUTHOR_URL = '{slug}'
AUTHOR_SAVE_AS = '{slug}.html'

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%m}.html'


TYPOGRIFY = True

WITH_FUTURE_DATES = False

PYGMENTS_RST_OPTIONS = {'classprefix': 'pgcss', 'linenos': 'table'}
